       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DOCALC.
       AUTHOR. Thanawat Weerasathian.
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01 FIRSTNUM    PIC 9     VALUE ZEROS.
       01 SECONDNUM   PIC 9     VALUE ZEROS.
       01 CALCRESULT  PIC 99    VALUE 0.
       01 USERPROMPT  PIC X(38) VALUE
             "Please enter two single digit numbers".
       PROCEDURE DIVISION .
       CALCULATERESULT.
           DISPLAY USERPROMPT 
           ACCEPT FIRSTNUM 
           ACCEPT SECONDNUM 
           COMPUTE CALCRESULT = FIRSTNUM + SECONDNUM 
           DISPLAY "Result is = ", CALCRESULT 
           STOP RUN.