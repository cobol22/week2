       IDENTIFICATION DIVISION. 
       PROGRAM-ID. COBOLGREETING.
       *>Program to display COBOL greetings
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 ITERNUM  PIC 9 VALUE 5.
      
       PROCEDURE DIVISION .
       BEGINPROGRAM.
           PERFORM DISPLAYGREETING ITERNUM TIMES.
           STOP RUN.
             
       DISPLAYGREETING.
           DISPLAY "Greetings from COBOL".  